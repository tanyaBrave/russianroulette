package org.levelup.annotations;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ClassFinderTest {

    private static List<Object> objects = new ArrayList<>();

    @Test
    @DisplayName("Find two classes by dir and create two objects")
    public void testGetObject_allClassesFound_twoObjectsCreated() {

        String packageName = "org.levelup.awesomeclasses";
        objects = ClassFinder.getObject(packageName);
        Assertions.assertEquals(2, objects.size());

    }

    @Test
    @DisplayName("All fields with RandomInt annotation get number")
    public void testSetField_fieldHasRandomIntAnnotation_fieldGetRandomNumber() throws IllegalAccessException {

        String packageName = "org.levelup.awesomeclasses";
        objects = ClassFinder.getObject(packageName);
        for (Object object: objects) {
          RandomIntAnnotationProcessor.setField(object);
        }



    }
}
