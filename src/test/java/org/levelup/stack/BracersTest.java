package org.levelup.stack;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class BracersTest {

    private Bracers bracers;

    @BeforeEach
    public void setup() {
        this.bracers = new Bracers();
    }

    @Test
   // testMethodName_when_then
    @DisplayName("When value is null, then throw exception")
    public void testVerify_valueIsNull_throwException() {
        String value = null;
        Assertions.assertThrows(IllegalArgumentException.class, () -> bracers.verify(value));
    }

    @Test
    public void testVerify_validString_returnTrue() {
        boolean result = bracers.verify("[]");
        Assertions.assertTrue(result);
    }

//    JUnit 4
//    @Test(expected = IllegalArgumentException.class)
//    public void testVerify_valueIsNull_throwException() {
//        bracers.verify(null);
//    }



}
