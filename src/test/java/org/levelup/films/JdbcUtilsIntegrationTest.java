package org.levelup.films;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

public class JdbcUtilsIntegrationTest {

    private Connection connection;

    @Test
    @DisplayName("Open new connection")
    public void testCreateConnection_returnCreatedConnection() throws SQLException {
        connection = JdbcUtils.createConnection();
        assertNotNull(connection);
        assertFalse(connection.isClosed());
    }

    @Test
    @DisplayName("Print all data from film")
    public void testSelectQuery_validQuery_printResultSet() throws SQLException {
        connection = JdbcUtils.createConnection();

        JdbcUtils.selectQuery(connection, "select * from film");
    }

    @Test
    @DisplayName("Insert new data in table")
    public void testInsertQuery_validQuery_addNewDataInFilm() throws SQLException {
        connection = JdbcUtils.createConnection();

        JdbcUtils.insertQuery(connection, "insert into films_db.film (id, name, duration, genre, year)" +
                                                "values (6, 'Вернуться в 1988', 110, 'Сериал', 2015);");
    }

    @Test
    @DisplayName("Update data in table")
    public void testUpdateQuery_validQuery_updateDataInFilm() throws SQLException {
        connection = JdbcUtils.createConnection();

        JdbcUtils.updateQuery(connection, "update films_db.film set genre = 'Дорама' where id = 6;");
    }

    @Test
    @DisplayName("Delete data in table")
    public void testDeleteQuery_validQuery_deleteDataFromFilm() throws SQLException {
        connection = JdbcUtils.createConnection();

        JdbcUtils.deleteQuery(connection, "delete from film where name = 'Вернуться в 1988'");
    }

    @AfterEach
    public void closeConnection() throws SQLException {
        connection.close();
    }
}
