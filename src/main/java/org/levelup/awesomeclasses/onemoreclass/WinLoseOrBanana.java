package org.levelup.awesomeclasses.onemoreclass;

import org.levelup.annotations.RandomInt;

public class WinLoseOrBanana {

    @RandomInt(min = 1, max = 6)
    private int number;


    public void guess(int number) {
        if (number == this.number) {
            System.out.println("You win...but who is Banana?");
        }
    }

    public int getNumber() {
        return number;
    }
}
