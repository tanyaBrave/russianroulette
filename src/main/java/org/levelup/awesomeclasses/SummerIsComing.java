package org.levelup.awesomeclasses;

import org.levelup.annotations.RandomInt;

public class SummerIsComing {

    @RandomInt(min = 1, max = 6)
    private int yourWeight;


    public void guess(int number) {
        if (number == this.yourWeight) {
            System.out.println("You are on a diet...");
        }
    }

    public int getYourWeight() {
        return yourWeight;
    }
}
