package org.levelup.films;

import java.sql.*;

public class JdbcUtils {

    //load class Driver in memory
    static{
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private static final String MYSQL_URL =
            "jdbc:mysql://localhost:3306/films_db?useSSL=false&serverTimezone=UTC";

    //метод который отдает подключение
    public static Connection createConnection() throws SQLException {
        //create connection
        return DriverManager.getConnection(MYSQL_URL, "root", "root");
    }

    public static void selectQuery(Connection connection, String query) throws SQLException {
        try (Statement statement = connection.createStatement()) {
          ResultSet result = statement.executeQuery(query);
          while (result.next()) {
              int id = result.getInt(1);
              String name = result.getString(2);
              double duration = result.getDouble("duration");
              String genre = result.getString("genre");
              int year = result.getInt("year");

              System.out.println(id + " " + name + " " + duration + " " + genre + " " + year);

          }

        }
    }

    public static void insertQuery(Connection connection, String query) throws SQLException {
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(query);

        }
    }

    public static void updateQuery(Connection connection, String query) throws SQLException {
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(query);

        }
    }

    public static void deleteQuery(Connection connection, String query) throws SQLException {
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(query);

        }
    }
}
