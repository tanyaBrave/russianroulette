package org.levelup.annotations;

import java.lang.reflect.Field;
import java.util.Random;

public class RandomIntAnnotationProcessor {

    public static void setField(Object object) throws IllegalAccessException {
        // 1. Получаем объекта класса Class
        // 2. Находим все его поля
        // 3. Находим аннтоции RandomInt
        // 4. Устанавливаем значение

        // RussianRoulette.class
        // object - String -> Class<String>
        // object - Phone -> Class<Phone>
        Class<?> objectClass = object.getClass();
        Field[] fields = objectClass.getDeclaredFields();

        for (Field field: fields) {
            // Class<RandomInt>
            System.out.println("Field: " + field.getName());
            System.out.println("Field type: " + field.getType());
            RandomInt annotation = field.getAnnotation(RandomInt.class);
            System.out.println("Is null: " + (annotation == null));
            if (annotation != null) {
                try {
                if (field.getType().equals(Integer.class) || field.getType().equals(int.class)) {
                    // [1 .. 6]
                    // nextInt(6) - [0 .. 5]
                    // max - 6, min - 1
                    // max - min - [0 .. 5)
                    // max - min + 1 [0 .. 6)
                    // (max - min + 1) + 1 -> [1 .. 7)
                    int number = new Random().nextInt(annotation.max() - annotation.min() + 1) + annotation.min();
                    System.out.println("Number: " + number);
                    // field - number
                    // first parameter - object
                    // second parameter - value
                    field.setAccessible(true);
                    field.set(object, number);
                } else throw new AnnotationInvalidFieldTypeException();
                } catch (AnnotationInvalidFieldTypeException e) {
                    e.printStackTrace();
                }

            }

        }

    }

}
