package org.levelup.annotations;

import java.util.List;

public class Main {

    public static void main(String[] args) throws IllegalAccessException {
        RussianRoulette roulette = new RussianRoulette();
        RandomIntAnnotationProcessor.setField(roulette);
        System.out.println(roulette.getNumber());
        roulette.guess(4);

       String packageName = "org.levelup.awesomeclasses";

       List<Object> objects;
       objects = ClassFinder.getObject(packageName);
        for (Object o: objects) {
            RandomIntAnnotationProcessor.setField(o);
        }


    }

}
